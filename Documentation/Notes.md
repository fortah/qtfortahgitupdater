# Notes

## Tree Model

Tree Model example:

https://doc.qt.io/qt-6/qtwidgets-itemviews-simpletreemodel-example.html

## Resizing Columns of QTreeView

However, that does not resize the other columns proportionately. If you want to do that as well, you could handle it 
inside the resizeEvent of your parent thusly:

```
void QParent::resizeEvent(QResizeEvent *event) {
    table_view->setColumnWidth(0, this->width()/3);
    table_view->setColumnWidth(1, this->width()/3);
    table_view->setColumnWidth(2, this->width()/3);

    QMainWindow::resizeEvent(event);
}
```

QParent class is subclass of QMainWindow.
