#!/bin/bash

tty -s || exec xfce4-terminal -e "$0" "$@"

echo "Linking libraries..."

if [ ! -L "../Libraries" ] ; then
    ln -s "/home/Tools/Libraries" "../Libraries"
fi

echo "Done."
read -n 1 -s -r -p "(Press any key to end)"

