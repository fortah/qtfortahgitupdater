@echo off

echo Linking libraries...

set drive=C
set application=GitUpdater
set source=%drive%:\Development\Qt\Applications\%application%\Scripts
set destination=%drive%:\Tools

cd
mklink /d "%source%\..\Libraries" "%destination%\Libraries" 

echo Done.
pause
