#include <QJsonArray>
#include <QJsonObject>

#include "CollectionReader.hpp"

#include "../../Libraries/FortahGeneralLibrary/Data/JsonToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    CollectionReader::CollectionReader() = default;

    CollectionPtr CollectionReader::read(const QString& pFilePath) {
        QJsonDocument jsonDocument {GeneralLibrary::JsonToolkit::read(pFilePath)};
        return CollectionReader::readCollection(jsonDocument.object());
    }

    CollectionPtr CollectionReader::readCollection(const QJsonObject& pCollectionJsonObject) {
        CollectionPtr collection {Collection::createPtr()};
        CollectionReader::readGroup(pCollectionJsonObject, *collection);
        return collection;
    }

    void CollectionReader::readGroups(const QJsonArray& pGroupsJsonArray, Groups& pGroups) {
        for (const QJsonValue& groupJsonValue : pGroupsJsonArray) {
            GroupPtr group {Group::createPtr()};
            CollectionReader::readGroup(groupJsonValue.toObject(), *group);
            pGroups.append(group);
        }
    }

    void CollectionReader::readGroup(const QJsonObject& pGroupJsonObject, Group& pGroup) {
        CollectionReader::readEntity(pGroupJsonObject, pGroup);
        CollectionReader::readGroups(pGroupJsonObject["groups"].toArray(), pGroup.getGroupsForWriting());
        CollectionReader::readRepositories(pGroupJsonObject["repositories"].toArray(), pGroup.getRepositoriesForWriting());
    }

    void CollectionReader::readRepositories(const QJsonArray& pRepositoriesJsonArray, Repositories& pRepositories) {
        for (const QJsonValue& repositoryJsonValue : pRepositoriesJsonArray) {
            RepositoryPtr repository {Repository::createPtr()};
            CollectionReader::readRepository(repositoryJsonValue.toObject(), *repository);
            pRepositories.append(repository);
        }
    }

    void CollectionReader::readRepository(const QJsonObject& pRepositoryJsonObject, Repository& pRepository) {
        CollectionReader::readEntity(pRepositoryJsonObject, pRepository);
    }

    void CollectionReader::readEntity(const QJsonObject& pEntityJsonObject, Entity& pEntity) {
        pEntity.setName(pEntityJsonObject["name"].toString());
        pEntity.setUrl(pEntityJsonObject["url"].toString());
        pEntity.setPath(pEntityJsonObject["path"].toString());
        pEntity.setBranch(pEntityJsonObject["branch"].toString());
    }

} }
