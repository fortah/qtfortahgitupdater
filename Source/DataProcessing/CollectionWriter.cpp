#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "CollectionWriter.hpp"

#include "../../Libraries/FortahGeneralLibrary/Data/JsonToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    CollectionWriter::CollectionWriter() = default;

    void CollectionWriter::write(const Collection& pCollection, const QString& pFilePath) {
        QJsonDocument jsonDocument;
        CollectionWriter::writeCollection(pCollection, jsonDocument);
        GeneralLibrary::JsonToolkit::write(jsonDocument, pFilePath);
    }

    void CollectionWriter::writeCollection(const Collection& pCollection, QJsonDocument& pJsonDocument) {
        QJsonObject collectionJsonObject;
        CollectionWriter::writeGroup(pCollection, collectionJsonObject);
        pJsonDocument.setObject(collectionJsonObject);
    }

    QJsonArray CollectionWriter::writeGroups(const Groups& pGroups) {
        QJsonArray groupsJsonArray;
        for (const GroupPtr& group : pGroups) {
            QJsonObject groupJsonObject;
            CollectionWriter::writeGroup(*group, groupJsonObject);
            groupsJsonArray.append(groupJsonObject);
        }
        return groupsJsonArray;
    }

    void CollectionWriter::writeGroup(const Group& pGroup, QJsonObject& pGroupJsonObject) {
        CollectionWriter::writeEntity(pGroup, pGroupJsonObject);
        GeneralLibrary::JsonToolkit::set(pGroupJsonObject, "groups", CollectionWriter::writeGroups(pGroup.getGroups()));
        GeneralLibrary::JsonToolkit::set(pGroupJsonObject, "repositories", CollectionWriter::writeRepositories(pGroup.getRepositories()));
    }

    QJsonArray CollectionWriter::writeRepositories(const Repositories& pRepositories) {
        QJsonArray repositoriesJsonArray;
        for (const RepositoryPtr& repository : pRepositories) {
            QJsonObject repositoryJsonObject;
            CollectionWriter::writeRepository(*repository, repositoryJsonObject);
            repositoriesJsonArray.append(repositoryJsonObject);
        }
        return repositoriesJsonArray;
    }

    void CollectionWriter::writeRepository(const Repository& pRepository, QJsonObject &pRepositoryJsonObject) {
        CollectionWriter::writeEntity(pRepository, pRepositoryJsonObject);
    }

    void CollectionWriter::writeEntity(const Entity& pEntity, QJsonObject& pEntityJsonObject) {
        GeneralLibrary::JsonToolkit::set(pEntityJsonObject, "name", pEntity.getName());
        GeneralLibrary::JsonToolkit::set(pEntityJsonObject, "url", pEntity.getUrl());
        GeneralLibrary::JsonToolkit::set(pEntityJsonObject, "path", pEntity.getPath());
    }

} }
