#pragma once

#include <QJsonObject>

#include "Data/Collection.hpp"

namespace Fortah {

    namespace GitUpdater {

        class CollectionReader {

            private:
                CollectionReader();

            public:
                static CollectionPtr read(const QString& pFilePath);

            private:
                static CollectionPtr readCollection(const QJsonObject& pGitCollectionJsonObject);
                static void readGroups(const QJsonArray& pGitGroupsJsonArray, Groups& pGroups);
                static void readGroup(const QJsonObject& pGroupJsonObject, Group& pGroup);
                static void readRepositories(const QJsonArray& pRepositoriesJsonArray, Repositories& pRepositories);
                static void readRepository(const QJsonObject& pRepositoryJsonObject, Repository& pRepository);
                static void readEntity(const QJsonObject& pEntityJsonObject, Entity& pEntity);

        };

    }

}
