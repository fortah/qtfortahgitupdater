#include "CollectionFactory.hpp"

namespace Fortah { namespace GitUpdater {

    CollectionFactory::CollectionFactory() = default;

    CollectionPtr CollectionFactory::create() {
        CollectionPtr collection {Collection::createPtr()};
        collection->setName("New Collection");
        collection->setModified(true);
        return collection;
    }

} }
