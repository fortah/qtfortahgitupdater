#pragma once

#include <QJsonDocument>
#include <QString>

#include "Data/Collection.hpp"

namespace Fortah { namespace GitUpdater {

    class CollectionWriter {

        private:
            CollectionWriter();

        public:
            static void write(const Collection& pCollection, const QString& pFilePath);

        private:
            static void writeCollection(const Collection& pCollection, QJsonDocument& pJsonDocument);
            static QJsonArray writeGroups(const Groups& pGroups);
            static void writeGroup(const Group& pGroup, QJsonObject& pGroupJsonObject);
            static QJsonArray writeRepositories(const Repositories& pRepositories);
            static void writeRepository(const Repository& pRepository, QJsonObject& pRepositoryJsonObject);
            static void writeEntity(const Entity& pEntity, QJsonObject& pEntityObject);

    };

} }
