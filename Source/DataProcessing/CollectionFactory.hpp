#pragma once

#include "Data/Collection.hpp"

namespace Fortah { namespace GitUpdater {

    class CollectionFactory {

        private:
            CollectionFactory();

        public:
            static CollectionPtr create();

    };

} }
