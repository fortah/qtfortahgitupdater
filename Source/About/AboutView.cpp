#include "AboutView.hpp"
#include "ui_AboutView.h"

#include "../General/Manifest.hpp"
#include "../Tools/InternetToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    AboutView::AboutView(QWidget* pParent) :
        AboutView(nullptr, pParent) {
    }

    AboutView::AboutView(AboutModelPtr pModel, QWidget* pParent) :
        QDialog {pParent},
        ui {new Ui::AboutView} {
        this->model = ((pModel != nullptr) ? (std::move(pModel)) : (AboutModel::createPtr()));
        this->ui->setupUi(this);
        this->initialise();
    }

    AboutView::~AboutView() {
        if (this->ui != nullptr)
            delete this->ui;
    }

    void AboutView::initialise() {
        this->setWindowTitle(this->model->getTitle());

        this->ui->companyLabel->setText(this->model->getCompany());
        this->ui->nameLabel->setText(this->model->getName());
        this->ui->descriptionLabel->setText(this->model->getDescription());
        this->ui->versionLabel->setText(this->model->getVersion().toString());
        this->ui->authorPushButton->setText(this->model->getAuthor().toString());
        this->ui->dateLabel->setText(this->model->getDate());
        this->ui->websitePushButton->setText(this->model->getWebsite());

        QObject::connect(this->ui->authorPushButton, &QPushButton::clicked, this, &AboutView::onAuthorPushButtonClicked);
        QObject::connect(this->ui->websitePushButton, &QPushButton::clicked, this, &AboutView::onWebsitePushButtonClicked);
    }

    void AboutView::onAuthorPushButtonClicked() {
        InternetToolkit::navigateToEmail(Manifest::getAuthor().getEmail());
    }

    void AboutView::onWebsitePushButtonClicked() {
        InternetToolkit::navigateToWebsite(Manifest::getWebsite());
    }

} }
