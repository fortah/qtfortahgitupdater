#pragma once

#include <QSharedPointer>
#include <QString>

#include "../../Libraries/FortahGeneralLibrary/Misc/Person.hpp"
#include "../../Libraries/FortahGeneralLibrary/General/Version.hpp"

namespace Fortah { namespace GitUpdater {

    class AboutModel;
    typedef QSharedPointer<AboutModel> AboutModelPtr;

    class AboutModel {

        public:
            AboutModel();
            static AboutModelPtr createPtr();

        public:
            const QString getTitle() const;
            const QString getCompany();
            const QString getName();
            const QString getDescription();
            const GeneralLibrary::Version getVersion();
            const GeneralLibrary::Person getAuthor();
            const QString getDate();
            const QString getWebsite();

    };

} }
