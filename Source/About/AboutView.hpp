#pragma once

#include <QDialog>
#include <QSharedPointer>

#include "About/AboutModel.hpp"

namespace Fortah { namespace GitUpdater {

    namespace Ui {
        class AboutView;
    }

    class AboutView : public QDialog {

        Q_OBJECT

        public:
            explicit AboutView(QWidget* pParent = nullptr);
            AboutView(AboutModelPtr pModel, QWidget* pParent = nullptr);
            ~AboutView();

        private:
            AboutModelPtr model {nullptr};
            Ui::AboutView* ui {nullptr};

        private:
            void initialise();

        private slots:
            void onAuthorPushButtonClicked();
            void onWebsitePushButtonClicked();
    };

} }
