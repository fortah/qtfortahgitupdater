#include "AboutModel.hpp"

#include "../General/Manifest.hpp"

namespace Fortah { namespace GitUpdater {

    AboutModel::AboutModel() = default;

    AboutModelPtr AboutModel::createPtr() {
        return AboutModelPtr{new AboutModel};
    }

    const QString AboutModel::getTitle() const {
        return (QString{"About %1"}).arg(Manifest::toString());
    }

    const QString AboutModel::getCompany() {
        return Manifest::getCompany();
    }

    const QString AboutModel::getName() {
        return Manifest::getName();
    }

    const QString AboutModel::getDescription() {
        return Manifest::getDescription();
    }

    const GeneralLibrary::Version AboutModel::getVersion() {
        return Manifest::getVersion();
    }

    const GeneralLibrary::Person AboutModel::getAuthor() {
        return Manifest::getAuthor();
    }

    const QString AboutModel::getDate() {
        return Manifest::getDate();
    }

    const QString AboutModel::getWebsite() {
        return Manifest::getWebsite();
    }

} }
