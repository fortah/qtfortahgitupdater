#include "Group.hpp"

namespace Fortah { namespace GitUpdater {

    Group::Group() = default;

    Group::~Group() = default;

    GroupPtr Group::createPtr() {
        return GroupPtr{new Group};
    }

    EntityType Group::getType() const {
        return EntityType::Group;
    }

    const Groups& Group::getGroups() const {
        return this->groups;
    }

    const Repositories& Group::getRepositories() const {
        return this->repositories;
    }

    Groups& Group::getGroupsForWriting() {
        return this->groups;
    }

    Repositories& Group::getRepositoriesForWriting() {
        return this->repositories;
    }

} }


