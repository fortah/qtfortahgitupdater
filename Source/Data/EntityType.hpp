#pragma once

#include "../../Libraries/FortahGeneralLibrary/General/EnumEx.hpp"

namespace Fortah { namespace GitUpdater {

    enum class EntityType {
        Undefined,
        Collection,
        Group,
        Repository
    };

    class EntityTypeEx {

        private:
            EntityTypeEx();

        public:
            static QString format(const EntityType pEntityType);
            static EntityType parse(const QString& pString);

        private:
            static GeneralLibrary::EnumEx<EntityType> getEnumEx();

    };

} }
