#pragma once

#include <QString>

#include "EntityResultType.hpp"

namespace Fortah { namespace GitUpdater {

    class EntityResult {

        public:
            EntityResult();
            EntityResult(const EntityResultType pType);
            EntityResult(const EntityResultType pType, const QString& pMessage);

        private:
            EntityResultType type;
            QString message;

        public:
            EntityResultType getType() const;
            const QString& getMessage() const;

        public:
            void setType(const EntityResultType pType);
            void setMessage(const QString& pMessage);

        public:
            QString getTypeText() const;
            QString toString() const;

    };

} }

