#include "EntityResult.hpp"

namespace Fortah { namespace GitUpdater {

    EntityResult::EntityResult() {
    }

    EntityResult::EntityResult(const EntityResultType pType) :
        type {pType} {
    }

    EntityResult::EntityResult(const EntityResultType pType, const QString& pMessage) :
        type {pType},
        message {pMessage} {
    }

    EntityResultType EntityResult::getType() const {
        return this->type;
    }

    const QString &EntityResult::getMessage() const {
        return this->message;
    }

    void EntityResult::setType(const EntityResultType pType) {
        this->type = pType;
    }

    void EntityResult::setMessage(const QString &pMessage) {
        this->message = pMessage;
    }

    QString EntityResult::getTypeText() const {
        return EntityResultTypeEx::format(this->type);
    }

    QString EntityResult::toString() const {
        if (!this->message.isEmpty())
            return QString{"%1: %2"}.arg(this->getTypeText(), this->message);
        else
            return this->getTypeText();
    }

} }

