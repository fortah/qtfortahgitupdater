#pragma once

#include <QMap>
#include <QString>

#include "../../Libraries/FortahGeneralLibrary/General/EnumEx.hpp"

namespace Fortah { namespace GitUpdater {

    enum class EntityResultType {
        Undefined = 0,
        Success = 1,
        Error = 2
    };

    class EntityResultTypeEx {

        private:
            EntityResultTypeEx();

        public:
            static QString format(const EntityResultType pValue);
            static EntityResultType parse(const QString& pString);

        private:
            static GeneralLibrary::EnumEx<EntityResultType> getEnumEx();

    };

} }

