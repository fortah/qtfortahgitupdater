#pragma once

#include <QSharedPointer>
#include <QString>

#include "EntityResult.hpp"
#include "EntityType.hpp"

namespace Fortah { namespace GitUpdater {

    class Entity;
    typedef QSharedPointer<Entity> EntityPtr;

    class Entity {

        protected:
            Entity();
            virtual ~Entity();

        protected:
            QString name {""};
            QString url {""};
            QString path {""};
            QString branch {""};
            EntityResult result {};

        public:
            virtual EntityType getType() const = 0;

        public:
            const QString& getName() const;
            const QString& getUrl() const;
            const QString& getPath() const;
            const QString& getBranch() const;
            const EntityResult& getResult() const;

        public:
            void setName(const QString& pName);
            void setUrl(const QString& pUrl);
            void setPath(const QString& pPath);
            void setBranch(const QString& pBranch);
            EntityResult& getResultForWriting();

    };

} }
