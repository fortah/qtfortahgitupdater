#include "Entity.hpp"

namespace Fortah { namespace GitUpdater {

    Entity::Entity() = default;

    Entity::~Entity() = default;

    const QString& Entity::getName() const {
        return this->name;
    }

    const QString& Entity::getUrl() const {
        return this->url;
    }

    const QString& Entity::getPath() const {
        return this->path;
    }

    const QString& Entity::getBranch() const {
        return this->branch;
    }

    const EntityResult& Entity::getResult() const {
        return this->result;
    }

    void Entity::setName(const QString& pName) {
        this->name = pName;
    }

    void Entity::setUrl(const QString& pUrl) {
        this->url = pUrl;
    }

    void Entity::setPath(const QString& pPath) {
        this->path = pPath;
    }

    void Entity::setBranch(const QString& pBranch) {
        this->branch = pBranch;
    }

    EntityResult& Entity::getResultForWriting() {
        return this->result;
    }

} }
