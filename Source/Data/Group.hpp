#pragma once

#include <QSharedPointer>
#include <QString>
#include <QVector>

#include "Entity.hpp"
#include "Repository.hpp"

namespace Fortah { namespace GitUpdater {

    class Group;
    typedef QSharedPointer<Group> GroupPtr;
    typedef QVector<GroupPtr> Groups;

    class Group : public Entity {

        public:
            Group();
            ~Group();
            static GroupPtr createPtr();

        private:
            Groups groups {};
            Repositories repositories {};

        public:
            EntityType getType() const override;

        public:
            const Groups& getGroups() const;
            const Repositories& getRepositories() const;

        public:
            Groups& getGroupsForWriting();
            Repositories& getRepositoriesForWriting();

    };

} }
