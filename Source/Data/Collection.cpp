#include "Collection.hpp"

namespace Fortah { namespace GitUpdater {

    Collection::Collection() = default;

    Collection::~Collection() = default;

    CollectionPtr Collection::createPtr() {
        return CollectionPtr{new Collection};
    }

    EntityType Collection::getType() const {
        return EntityType::Collection;
    }

    const QString& Collection::getFilePath() const {
        return this->filePath;
    }

    bool Collection::isModified() const {
        return this->modified;
    }

    void Collection::setFilePath(const QString& pFilePath) {
        this->filePath = pFilePath;
    }

    void Collection::setModified(const bool pModified) {
        this->modified = pModified;
    }

} }
