#include <QMap>
#include <QString>

#include "EntityType.hpp"

namespace Fortah { namespace GitUpdater {

    EntityTypeEx::EntityTypeEx() = default;

    QString EntityTypeEx::format(const EntityType pEntityType)  {
        return EntityTypeEx::getEnumEx().format(pEntityType);
    }

    EntityType EntityTypeEx::parse(const QString& pString) {
        return EntityTypeEx::getEnumEx().parse(pString, EntityType::Undefined);
    }

    GeneralLibrary::EnumEx<EntityType> EntityTypeEx::getEnumEx() {
        return GeneralLibrary::EnumEx<EntityType>{
            QMap<EntityType, QString>{
                {EntityType::Undefined, ""},
                {EntityType::Collection, "Collection"},
                {EntityType::Group, "Group"},
                {EntityType::Repository, "Repository"}
            }
        };
    }

} }
