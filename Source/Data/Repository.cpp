#include "Repository.hpp"

namespace Fortah { namespace GitUpdater {

    Repository::Repository() = default;
    Repository::~Repository() = default;

    RepositoryPtr Repository::createPtr() {
        return RepositoryPtr{new Repository};
    }

    EntityType Repository::getType() const {
        return EntityType::Repository;
    }

} }
