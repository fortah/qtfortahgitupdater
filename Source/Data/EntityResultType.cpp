#include "EntityResultType.hpp"

namespace Fortah { namespace GitUpdater {

    EntityResultTypeEx::EntityResultTypeEx() = default;

    QString EntityResultTypeEx::format(const EntityResultType pValue) {
        return EntityResultTypeEx::getEnumEx().format(pValue);
    }

    EntityResultType EntityResultTypeEx::parse(const QString& pString) {
        return EntityResultTypeEx::getEnumEx().parse(pString, EntityResultType::Undefined);
    }

    GeneralLibrary::EnumEx<EntityResultType> EntityResultTypeEx::getEnumEx() {
        return GeneralLibrary::EnumEx<EntityResultType>{
            QMap<EntityResultType, QString>{
                {EntityResultType::Undefined, ""},
                {EntityResultType::Success, "Success"},
                {EntityResultType::Error, "Error"}
            }
        };
    }

} }

