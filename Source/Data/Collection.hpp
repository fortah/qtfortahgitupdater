#pragma once

#include <QSharedPointer>

#include "Group.hpp"

namespace Fortah { namespace GitUpdater {

    class Collection;
    typedef QSharedPointer<Collection> CollectionPtr;

    class Collection : public Group {

        public:
            Collection();
            ~Collection();
            static CollectionPtr createPtr();

        private:
            QString filePath {""};
            bool modified {false};

        public:
            EntityType getType() const override;

        public:
            const QString& getFilePath() const;
            bool isModified() const;

        public:
            void setFilePath(const QString& pFilePath);
            void setModified(const bool pModified);

    };

} }
