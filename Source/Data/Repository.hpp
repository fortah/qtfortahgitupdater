#pragma once

#include <QSharedPointer>
#include <QVector>

#include "Entity.hpp"

namespace Fortah {

    namespace GitUpdater {

        class Repository;
        typedef QSharedPointer<Repository> RepositoryPtr;
        typedef QVector<RepositoryPtr> Repositories;

        class Repository : public Entity {

            public:
                Repository();
                ~Repository();
                static RepositoryPtr createPtr();

            public:
                EntityType getType() const override;

        };

    }

}
