#include "Manifest.hpp"

namespace Fortah { namespace GitUpdater {

    Manifest::Manifest() = default;

    const QString Manifest::getCompany() {
        return "Fortah";
    }

    const QString Manifest::getName() {
        return "Git Updater";
    }

    const QString Manifest::getDescription() {
        return "Automatically updates Git repositories";
    }

    const GeneralLibrary::Version Manifest::getVersion() {
        return GeneralLibrary::Version{0, 0, 1, 0};
    }

    const GeneralLibrary::Person Manifest::getAuthor() {
        return GeneralLibrary::Person{"Jakub", "Hojnacki", "jakubhojnacki@gmail.com"};
    }

    const QString Manifest::getDate() {
        return "August 2024";
    }

    const QString Manifest::getWebsite() {
        return "https://gitlab.com/fortah/qtfortahgitupdater";
    }

    const QString Manifest::toString() {
        return (QString{"%1 %2 %3"}).arg(Manifest::getCompany(), Manifest::getName(), Manifest::getVersion().toString());
    }

} }
