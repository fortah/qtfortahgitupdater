#pragma once

#include <QApplication>

#include "../Main/MainView.hpp"

namespace Fortah { namespace GitUpdater {

    class Application : QApplication {

        public:
            Application(int pArgC, char* pArgV[]);
            ~Application();

        private:
            MainView* mainView {nullptr};

        public:
            int run();

    };

} }

