#pragma once

#include <QString>

#include "../../Libraries/FortahGeneralLibrary/Misc/Person.hpp"
#include "../../Libraries/FortahGeneralLibrary/General/Version.hpp"

namespace Fortah { namespace GitUpdater {

    class Manifest {

        protected:
            Manifest();

        public:
            static const QString getCompany();
            static const QString getName();
            static const QString getDescription();
            static const GeneralLibrary::Version getVersion();
            static const GeneralLibrary::Person getAuthor();
            static const QString getDate();
            static const QString getWebsite();

        public:
            static const QString toString();

    };

} }
