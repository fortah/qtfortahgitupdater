#pragma once

namespace Fortah { namespace GitUpdater {

    enum class MainMode {
        Undefined,
        NoCollection,
        Collection
    };

} }
