#pragma once

#include <QLabel>
#include <QMainWindow>
#include <QSharedPointer>

#include "MainLogic.hpp"
#include "MainModel.hpp"
#include "MainStatusBarHandle.hpp"

namespace Fortah { namespace GitUpdater {

    namespace Ui {
        class MainView;
    }

    class MainView : public QMainWindow {

        Q_OBJECT

        public:
            MainView(QWidget* pParent = nullptr);
            MainView(MainModelPtr pModel, QWidget* pParent = nullptr);
            ~MainView();

        private:
            MainModelPtr model {nullptr};
            Ui::MainView* ui {nullptr};
            MainStatusBarHandle statusBarHandle {};

        private:
            void initialise();
            void initialiseActions();
            void update();
            void updateMainView();
            void updateActions();
            void updateStatusBar();

        private:
            MainLogic createLogic();

        private slots:
            void onCreateActionTriggered();
            void onOpenActionTriggered();
            void onSaveActionTriggered();
            void onSaveAsActionTriggered();
            void onCloseActionTriggered();
            void onExitActionTriggered();
            void onCutActionTriggered();
            void onCopyActionTriggered();
            void onPasteActionTriggered();
            void onAddActionTriggered();
            void onDeleteActionTriggered();
            void onAboutActionTriggered();
            void onWebsiteActionTriggered();

    };

} }

