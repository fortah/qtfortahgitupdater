#include <QCoreApplication>
#include <QFileDialog>
#include <QMessageBox>

#include "MainLogic.hpp"

#include "../DataProcessing/CollectionFactory.hpp"
#include "../DataProcessing/CollectionReader.hpp"
#include "../DataProcessing/CollectionWriter.hpp"

namespace Fortah { namespace GitUpdater {

    MainLogic::MainLogic(MainModel* pModel, QWidget* pView) :
        model {pModel},
        view {pView} {
    }

    bool MainLogic::create() const {
        bool result {false};
        if (this->confirmNotSaved()) {
            this->model->setCollection(CollectionFactory::create());
            result = true;
        }
        return result;
    }

    bool MainLogic::open() const {
        bool result {false};
        if (this->confirmNotSaved()) {
            QString filePath {QFileDialog::getOpenFileName(this->view, "Open Git Collection", "", MainLogic::getFilter())};
            if (!filePath.isNull()) {
                QSharedPointer<Collection> collection {CollectionReader::read(filePath)};
                collection->setFilePath(filePath);
                this->model->setCollection(collection);
                result = true;
            }
        }
        return result;
    }

    bool MainLogic::save() const {
        bool result {false};
        CollectionPtr collection {this->model->getCollection()};
        if ((collection != nullptr) && (collection->isModified())) {
            CollectionWriter::write(*collection, collection->getFilePath());
            collection->setModified(false);
            result = true;
        }
        return result;
    }

    bool MainLogic::saveAs() const {
        bool result {false};
        CollectionPtr collection {this->model->getCollection()};
        if (collection != nullptr) {
            QString filePath {QFileDialog::getSaveFileName(this->view, "Save Git Collection as", "", MainLogic::getFilter())};
            if (!filePath.isNull()) {
                CollectionWriter::write(*collection, filePath);
                collection->setFilePath(filePath);
                collection->setModified(false);
                result = true;
            }
        }
        return result;
    }

    bool MainLogic::close() const {
        bool result {false};
        if (this->confirmNotSaved()) {
            this->model->setCollection(nullptr);
            result = true;
        }
        return result;
    }

    bool MainLogic::exit() const {
        bool result {false};
        if (this->confirmNotSaved()) {
            QCoreApplication::quit();
            result = true;
        }
        return result;
    }

    const QString MainLogic::getFilter() {
        return "Git Collection Files (*.gco);;JSON Files (*.json);;All Files (*.*)";
    }

    bool MainLogic::confirmNotSaved() const {
        bool result {false};
        CollectionPtr collection {this->model->getCollection()};
        if ((collection != nullptr) && (collection->isModified())) {
            QString questionTemplate {"Git collection '%1' hasn't been saved. This action will destroy all non-saved modifications. Do you want to continue?"};
            QString question {questionTemplate.arg(collection->getName())};
            QMessageBox::StandardButton reply {QMessageBox::question(this->view, "Git Collection Not Saved", question, QMessageBox::Yes|QMessageBox::No)};
            result = (reply == QMessageBox::Yes);
        } else
            result = true;
        return result;
    }

} }
