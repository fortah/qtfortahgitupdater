#pragma once

#include <QSharedPointer>
#include <QString>

#include "MainMode.hpp"

#include "../Data/Collection.hpp"

namespace Fortah { namespace GitUpdater {

    class MainModel;
    typedef QSharedPointer<MainModel> MainModelPtr;

    class MainModel {

        public:
            MainModel();
            static MainModelPtr createPtr();
            virtual ~MainModel();

        private:
            CollectionPtr collection {nullptr};
            MainMode mode {MainMode::Undefined};

        public:
            CollectionPtr getCollection() const;
            MainMode getMode() const;

        public:
            void setCollection(const CollectionPtr& pCollection);
            void setMode(const MainMode pMode);

        public:
            const QString getTitle() const;
            const QString getStatusBarName() const;
            const QString getStatusBarFileName() const;

        public:
            MainMode workOutNewMode() const;

        public:
            bool isCreateActionEnabled() const;
            bool isOpenActionEnabled() const;
            bool isSaveActionEnabled() const;
            bool isSaveAsActionEnabled() const;
            bool isCloseActionEnabled() const;
            bool isExitActionEnabled() const;
            bool isCutActionEnabled() const;
            bool isCopyActionEnabled() const;
            bool isPasteActionEnabled() const;
            bool isAddActionEnabled() const;
            bool isDeleteActionEnabled() const;
            bool isAboutActionEnabled() const;
            bool isWebsiteActionEnabled() const;

    };

} }
