#include "MainCollectionView.hpp"
#include "ui_MainCollectionView.h"

#include "../Tree/CollectionTreeFactory.hpp"
#include "../Tree/TreeToolkit.hpp"

#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumns.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeItem.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeModel.hpp"

namespace Fortah { namespace GitUpdater {

    MainCollectionView::MainCollectionView(QWidget* pParent) :
        MainCollectionView(nullptr, pParent) {
    }

    MainCollectionView::MainCollectionView(const MainModel* pModel, QWidget* pParent) :
        QWidget(pParent),
        model {pModel},
        ui {new Ui::MainCollectionView} {
        this->ui->setupUi(this);
        this->initialise();
    }

    MainCollectionView::~MainCollectionView() {
        if (this->ui != nullptr)
            delete this->ui;
    }

    void MainCollectionView::initialise() {
        if (this->model != nullptr) {
            CollectionPtr collection {this->model->getCollection()};
            if (collection != nullptr) {
                GeneralLibrary::TreeItemPtr root {CollectionTreeFactory::create(collection, this->columns)};
                GeneralLibrary::TreeModel* model {new GeneralLibrary::TreeModel{root, this}};
                this->ui->collectionTreeView->setModel(model);
                TreeToolkit::adjustColumns(*this->ui->collectionTreeView, this->columns);
                this->ui->collectionTreeView->expandAll();
            }
        }

    }

    void MainCollectionView::resizeEvent(QResizeEvent* pEvent) {
        TreeToolkit::adjustColumns(*this->ui->collectionTreeView, this->columns);
        QWidget::resizeEvent(pEvent);
    }

} }
