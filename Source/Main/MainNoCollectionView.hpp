#pragma once

#include <QWidget>

namespace Fortah { namespace GitUpdater {

    namespace Ui {
        class MainNoCollectionView;
    }

    class MainNoCollectionView : public QWidget {

        Q_OBJECT

        public:
            explicit MainNoCollectionView(QWidget* pParent = nullptr);
            ~MainNoCollectionView();

        private:
            Ui::MainNoCollectionView* ui {nullptr};

    };

} }

