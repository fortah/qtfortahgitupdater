#pragma once

#include <QWidget>

#include "MainModel.hpp"

namespace Fortah { namespace GitUpdater {

    class MainLogic {

        public:
            MainLogic(MainModel* pModel, QWidget* pView);

        private:
            MainModel* model {nullptr};
            QWidget* view {nullptr};

        public:
            bool create() const;
            bool open() const;
            bool save() const;
            bool saveAs() const;
            bool close() const;
            bool exit() const;

        private:
            static const QString getFilter();
            bool confirmNotSaved() const;

    };

} }
