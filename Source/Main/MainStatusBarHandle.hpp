#pragma once

#include <QLabel>

namespace Fortah { namespace GitUpdater {

    class MainStatusBarHandle {

        public:
            MainStatusBarHandle();

        private:
            QLabel* nameLabel {nullptr};
            QLabel* filePathLabel {nullptr};

        public:
            const QLabel* getNameLabel() const;
            const QLabel* getFilePathLabel() const;

        public:
            void setNameLabel(QLabel* pNameLabel);
            void setFilePathLabel(QLabel* pFilePathLabel);

        public:
            void updateNameLabel(const QString& pText);
            void updateFilePathLabel(const QString& pText);

    };

} }
