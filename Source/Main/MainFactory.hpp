#pragma once

#include <QStatusBar>
#include <QWidget>

#include "MainModel.hpp"
#include "MainStatusBarHandle.hpp"

namespace Fortah { namespace GitUpdater {

    class MainFactory {

        protected:
            MainFactory();

        public:
            static void createView(const MainModel& pModel, QWidget* pMainWidget);
            static MainStatusBarHandle createStatusBarWidgets(QStatusBar* pStatusBar);

        private:
            static QLabel* createStatusBarLabel(QStatusBar* pStatusBar);

    };

} }
