#include "MainModel.hpp"

#include "../General/Manifest.hpp"

namespace Fortah {

    namespace GitUpdater {

        MainModel::MainModel() = default;

        MainModelPtr MainModel::createPtr() {
            return MainModelPtr{new MainModel{}};
        }

        MainModel::~MainModel() = default;

        CollectionPtr MainModel::getCollection() const {
            return this->collection;
        }

        MainMode MainModel::getMode() const {
            return this->mode;
        }

        void MainModel::setCollection(const CollectionPtr& pGitCollection) {
            this->collection = pGitCollection;
        }

        void MainModel::setMode(const MainMode pMode) {
            this->mode = pMode;
        }

        const QString MainModel::getTitle() const {
            return Manifest::toString();
        }

        const QString MainModel::getStatusBarName() const {
            return ((this->collection != nullptr) ? (this->collection->getName()) : ("No Git Collection opened."));
        }

        const QString MainModel::getStatusBarFileName() const {
            return ((this->collection != nullptr) ? (this->collection->getFilePath()) : (""));
        }

        MainMode MainModel::workOutNewMode() const {
            return ((this->collection != nullptr) ? (MainMode::Collection) : (MainMode::NoCollection));
        }

        bool MainModel::isCreateActionEnabled() const {
            return true;
        }

        bool MainModel::isOpenActionEnabled() const {
            return true;
        }

        bool MainModel::isSaveActionEnabled() const {
            return ((this->collection != nullptr) && (this->collection->isModified()));
        }

        bool MainModel::isSaveAsActionEnabled() const {
            return this->collection != nullptr;
        }

        bool MainModel::isCloseActionEnabled() const {
            return this->collection != nullptr;
        }

        bool MainModel::isExitActionEnabled() const {
            return true;
        }

        bool MainModel::isCutActionEnabled() const {
            return false;
        }

        bool MainModel::isCopyActionEnabled() const {
            return false;
        }

        bool MainModel::isPasteActionEnabled() const {
            return false;
        }

        bool MainModel::isAddActionEnabled() const {
            return false;
        }

        bool MainModel::isDeleteActionEnabled() const {
            return false;
        }

        bool MainModel::isAboutActionEnabled() const {
            return true;
        }

        bool MainModel::isWebsiteActionEnabled() const {
            return true;
        }

    }

}
