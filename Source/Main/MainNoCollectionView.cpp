#include "MainNoCollectionView.hpp"
#include "ui_MainNoCollectionView.h"

namespace Fortah { namespace GitUpdater {

    MainNoCollectionView::MainNoCollectionView(QWidget* pParent) :
        QWidget(pParent),
        ui {new Ui::MainNoCollectionView} {
        this->ui->setupUi(this);
    }

    MainNoCollectionView::~MainNoCollectionView() {
        if (this->ui != nullptr)
            delete this->ui;
    }

} }
