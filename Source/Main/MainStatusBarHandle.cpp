#include "MainStatusBarHandle.hpp"

namespace Fortah { namespace GitUpdater {

    MainStatusBarHandle::MainStatusBarHandle() = default;

    const QLabel* MainStatusBarHandle::getNameLabel() const {
        return this->nameLabel;
    }

    const QLabel* MainStatusBarHandle::getFilePathLabel() const {
        return this->filePathLabel;
    }

    void MainStatusBarHandle::setNameLabel(QLabel* pNameLabel) {
        this->nameLabel = pNameLabel;
    }

    void MainStatusBarHandle::setFilePathLabel(QLabel* pFilePathLabel) {
        this->filePathLabel = pFilePathLabel;
    }

    void MainStatusBarHandle::updateNameLabel(const QString& pText) {
        if (this->nameLabel != nullptr)
            this->nameLabel->setText(pText);
    }

    void MainStatusBarHandle::updateFilePathLabel(const QString& pText) {
        if (this->filePathLabel != nullptr)
            this->filePathLabel->setText(pText);
    }

} }
