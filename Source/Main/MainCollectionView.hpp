#pragma once

#include <QWidget>

#include "MainModel.hpp"

#include "../Tree/CollectionTreeFactory.hpp"

namespace Fortah { namespace GitUpdater {

    namespace Ui {
        class MainCollectionView;
    }

    class MainCollectionView : public QWidget {

        Q_OBJECT

        public:
            explicit MainCollectionView(QWidget* pParent = nullptr);
            MainCollectionView(const MainModel* pModel, QWidget* pParent = nullptr);
            ~MainCollectionView();

        private:
            const MainModel* model {nullptr};
            Ui::MainCollectionView* ui {nullptr};
            GeneralLibrary::TreeColumns columns {CollectionTreeFactory::createColumns()};

        private:
            void initialise();

        protected:
            void resizeEvent(QResizeEvent* pEvent);

    };

} }
