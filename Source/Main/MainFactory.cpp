#include <QLayout>

#include "MainCollectionView.hpp"
#include "MainFactory.hpp"
#include "MainNoCollectionView.hpp"
#include "../Tools/WidgetToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    MainFactory::MainFactory() = default;

    void MainFactory::createView(const MainModel& pModel, QWidget* pMainWidget) {
        WidgetToolkit::removeAllChildren(pMainWidget);
        QWidget* newWidget;
        switch (pModel.getMode()) {
            case MainMode::NoCollection:
                newWidget = new MainNoCollectionView{pMainWidget};
                break;
            case MainMode::Collection:
                newWidget = new MainCollectionView{&pModel, pMainWidget};
                break;
            default:
                newWidget = nullptr;
                break;
        }
        if (newWidget != nullptr)
            pMainWidget->layout()->addWidget(newWidget);
    }

    MainStatusBarHandle MainFactory::createStatusBarWidgets(QStatusBar* pStatusBar) {
        MainStatusBarHandle mainStatusBarHandle;
        mainStatusBarHandle.setNameLabel(MainFactory::createStatusBarLabel(pStatusBar));
        mainStatusBarHandle.setFilePathLabel(MainFactory::createStatusBarLabel(pStatusBar));
        return mainStatusBarHandle;
    }

    QLabel *MainFactory::createStatusBarLabel(QStatusBar* pStatusBar) {
        QLabel* label = new QLabel{pStatusBar};
        pStatusBar->addWidget(label);
        return label;
    }

} }

