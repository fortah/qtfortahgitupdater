#include "MainView.hpp"
#include "ui_MainView.h"

#include "MainFactory.hpp"

#include "../About/AboutView.hpp"
#include "../General/Manifest.hpp"
#include "../Tools/InternetToolkit.hpp"
#include "../Tools/WidgetToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    MainView::MainView(QWidget* pParent) :
        MainView(nullptr, pParent) {
    }

    MainView::MainView(MainModelPtr pModel, QWidget* pParent) :
        QMainWindow(pParent),
        ui {new Ui::MainView} {
        this->model = ((pModel != nullptr) ? (pModel) : (MainModel::createPtr()));
        this->ui->setupUi(this);
        this->initialise();
        this->update();
    }

    MainView::~MainView() {
        if (this->ui != nullptr)
            delete this->ui;
    }

    void MainView::initialise() {
        this->setWindowTitle(this->model->getTitle());
        this->initialiseActions();
        this->statusBarHandle = MainFactory::createStatusBarWidgets(this->ui->statusBar);
    }

    void MainView::initialiseActions() {
        QObject::connect(this->ui->createAction, &QAction::triggered, this, &MainView::onCreateActionTriggered);
        QObject::connect(this->ui->openAction, &QAction::triggered, this, &MainView::onOpenActionTriggered);
        QObject::connect(this->ui->saveAction, &QAction::triggered, this, &MainView::onSaveActionTriggered);
        QObject::connect(this->ui->saveAsAction, &QAction::triggered, this, &MainView::onSaveAsActionTriggered);
        QObject::connect(this->ui->closeAction, &QAction::triggered, this, &MainView::onCloseActionTriggered);
        QObject::connect(this->ui->exitAction, &QAction::triggered, this, &MainView::onExitActionTriggered);
        QObject::connect(this->ui->cutAction, &QAction::triggered, this, &MainView::onCutActionTriggered);
        QObject::connect(this->ui->copyAction, &QAction::triggered, this, &MainView::onCopyActionTriggered);
        QObject::connect(this->ui->pasteAction, &QAction::triggered, this, &MainView::onPasteActionTriggered);
        QObject::connect(this->ui->addAction, &QAction::triggered, this, &MainView::onAddActionTriggered);
        QObject::connect(this->ui->deleteAction, &QAction::triggered, this, &MainView::onDeleteActionTriggered);
        QObject::connect(this->ui->aboutAction, &QAction::triggered, this, &MainView::onAboutActionTriggered);
        QObject::connect(this->ui->websiteAction, &QAction::triggered, this, &MainView::onWebsiteActionTriggered);
    }

    void MainView::update() {
        this->updateMainView();
        this->updateActions();
        this->updateStatusBar();
    }

    void MainView::updateMainView() {
        MainMode newMode {this->model->workOutNewMode()};
        if (newMode != this->model->getMode()) {
            this->model->setMode(newMode);
            MainFactory::createView(*this->model, this->ui->mainWidget);
        }
    }

    void MainView::updateActions() {
        this->ui->createAction->setEnabled(this->model->isCreateActionEnabled());
        this->ui->openAction->setEnabled(this->model->isOpenActionEnabled());
        this->ui->saveAction->setEnabled(this->model->isSaveActionEnabled());
        this->ui->saveAsAction->setEnabled(this->model->isSaveAsActionEnabled());
        this->ui->closeAction->setEnabled(this->model->isCloseActionEnabled());
        this->ui->exitAction->setEnabled(this->model->isExitActionEnabled());
        this->ui->cutAction->setEnabled(this->model->isCutActionEnabled());
        this->ui->copyAction->setEnabled(this->model->isCopyActionEnabled());
        this->ui->pasteAction->setEnabled(this->model->isPasteActionEnabled());
        this->ui->addAction->setEnabled(this->model->isAddActionEnabled());
        this->ui->deleteAction->setEnabled(this->model->isDeleteActionEnabled());
        this->ui->aboutAction->setEnabled(this->model->isAboutActionEnabled());
        this->ui->websiteAction->setEnabled(this->model->isWebsiteActionEnabled());
    }

    void MainView::updateStatusBar() {
        this->statusBarHandle.updateNameLabel(this->model->getStatusBarName());
        this->statusBarHandle.updateFilePathLabel(this->model->getStatusBarFileName());
    }

    MainLogic MainView::createLogic() {
        return MainLogic{this->model.data(), this};
    }

    void MainView::onCreateActionTriggered() {
        if (this->createLogic().create())
            this->update();
    }

    void MainView::onOpenActionTriggered() {
        if (this->createLogic().open())
            this->update();
    }

    void MainView::onSaveActionTriggered() {
        if (this->createLogic().save())
            this->update();
    }

    void MainView::onSaveAsActionTriggered() {
        if (this->createLogic().saveAs())
            this->update();
    }

    void MainView::onCloseActionTriggered() {
        if (this->createLogic().close())
            this->update();
    }

    void MainView::onExitActionTriggered() {
        this->createLogic().exit();
    }

    void MainView::onCutActionTriggered() {
        //TODO >>> Not implemented
    }

    void MainView::onCopyActionTriggered() {
        //TODO >>> Not implemented
    }

    void MainView::onPasteActionTriggered() {
        //TODO >>> Not implemented
    }

    void MainView::onAddActionTriggered() {
        //TODO >>> Not implemented
    }

    void MainView::onDeleteActionTriggered() {
        //TODO >>> Not implemented
    }

    void MainView::onAboutActionTriggered() {
        (AboutView{this}).exec();
    }

    void MainView::onWebsiteActionTriggered() {
        InternetToolkit::navigateToWebsite(Manifest::getWebsite());
    }

} }
