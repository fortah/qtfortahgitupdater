#pragma once

#include <QLayout>

namespace Fortah { namespace GitUpdater {

    class LayoutToolkit {

        private:
            LayoutToolkit();

        public:
            static void removeAllChildren(QLayout* pLayout);

    };

} }
