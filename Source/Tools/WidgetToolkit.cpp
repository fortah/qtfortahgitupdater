#include "LayoutToolkit.hpp"
#include "WidgetToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    WidgetToolkit::WidgetToolkit() = default;

    void WidgetToolkit::removeAllChildren(QWidget* pWidget) {
        if (pWidget != nullptr) {
            QLayout* layout = pWidget->layout();
            if (layout != nullptr)
                LayoutToolkit::removeAllChildren(layout);
        }
    }

} }

