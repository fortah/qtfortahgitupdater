#pragma once

#include <QWidget>

namespace Fortah { namespace GitUpdater {

    class WidgetToolkit {

        private:
            WidgetToolkit();

        public:
            static void removeAllChildren(QWidget* pWidget);

    };

} }
