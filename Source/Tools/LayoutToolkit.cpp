#include <QWidget>

#include "LayoutToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    LayoutToolkit::LayoutToolkit() = default;

    void LayoutToolkit::removeAllChildren(QLayout* pLayout) {
        while (QLayoutItem* item = pLayout->takeAt(0)) {
            if (QWidget* widget = item->widget())
                widget->deleteLater();
            if (QLayout* childLayout = item->layout())
                LayoutToolkit::removeAllChildren(childLayout);
            delete item;
        }
    }

} }


