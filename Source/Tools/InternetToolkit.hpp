#pragma once

#include <QString>

namespace Fortah { namespace GitUpdater {

    class InternetToolkit {

        private:
            InternetToolkit();

        public:
            static void navigateToWebsite(const QString& pAddress);
            static void navigateToEmail(const QString& pEmail);

    };

} }
