#include <QDesktopServices>
#include <QUrl>

#include "InternetToolkit.hpp"

namespace Fortah { namespace GitUpdater {

    InternetToolkit::InternetToolkit() = default;

    void InternetToolkit::navigateToWebsite(const QString& pAddress) {
        QUrl url {pAddress};
        QDesktopServices::openUrl(url);
    }

    void InternetToolkit::navigateToEmail(const QString& pEmail) {
        QUrl url {(QString{"mailto:%1"}).arg(pEmail)};
        QDesktopServices::openUrl(url);
    }

} }
