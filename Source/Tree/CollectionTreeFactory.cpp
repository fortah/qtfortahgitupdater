#include "CollectionTreeEntityData.hpp"
#include "CollectionTreeFactory.hpp"

#include "../../Libraries/FortahGeneralLibrary/General/Size.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumn.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumns.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeHeaderData.hpp"

namespace Fortah { namespace GitUpdater {

    CollectionTreeFactory::CollectionTreeFactory() = default;

    GeneralLibrary::TreeColumns CollectionTreeFactory::createColumns() {
        return GeneralLibrary::TreeColumns {
            GeneralLibrary::TreeColumn::createPtr("Name", GeneralLibrary::Size::createPercent(59)),
            GeneralLibrary::TreeColumn::createPtr("Result", GeneralLibrary::Size::createPercent(10)),
            GeneralLibrary::TreeColumn::createPtr("Message", GeneralLibrary::Size::createPercent(30))
        };
    }

    GeneralLibrary::TreeItemPtr CollectionTreeFactory::create(const CollectionPtr pCollection, const GeneralLibrary::TreeColumns& pColumns) {
        GeneralLibrary::TreeItemPtr rootTreeItem {CollectionTreeFactory::createHeaderTreeItemPtr(pColumns.getHeaders())};
        GeneralLibrary::TreeItemPtr collectionTreeItem {CollectionTreeFactory::addCollection(pCollection, *rootTreeItem)};
        CollectionTreeFactory::addGroups(pCollection->getGroups(), *collectionTreeItem);
        CollectionTreeFactory::addRepositories(pCollection->getRepositories(), *collectionTreeItem);
        return rootTreeItem;
    }

    GeneralLibrary::TreeItemPtr CollectionTreeFactory::addCollection(const CollectionPtr pCollection, GeneralLibrary::TreeItem& pParentTreeItem) {
        GeneralLibrary::TreeItemPtr treeItem {CollectionTreeFactory::createEntityTreeItemPtr(pCollection, &pParentTreeItem)};
        pParentTreeItem.appendChild(treeItem);
        return treeItem;
    }

    void CollectionTreeFactory::addGroups(const Groups& pGroups, GeneralLibrary::TreeItem& pParentTreeItem) {
        for (const GroupPtr& group : pGroups) {
            GeneralLibrary::TreeItemPtr treeItem {CollectionTreeFactory::createEntityTreeItemPtr(group, &pParentTreeItem)};
            CollectionTreeFactory::addGroups(group->getGroups(), *treeItem);
            CollectionTreeFactory::addRepositories(group->getRepositories(), *treeItem);
            pParentTreeItem.appendChild(treeItem);
        }
    }

    void CollectionTreeFactory::addRepositories(const Repositories& pRepositories, GeneralLibrary::TreeItem& pParentTreeItem) {
        for (const RepositoryPtr& repository : pRepositories)
            pParentTreeItem.appendChild(CollectionTreeFactory::createEntityTreeItemPtr(repository, &pParentTreeItem));
    }

    GeneralLibrary::TreeItemPtr CollectionTreeFactory::createHeaderTreeItemPtr(const QStringList& pHeaders) {
        GeneralLibrary::TreeDataPtr dataPtr {GeneralLibrary::TreeHeaderData::createPtr(pHeaders)};
        return GeneralLibrary::TreeItem::createPtr(dataPtr);
    }

    GeneralLibrary::TreeItemPtr CollectionTreeFactory::createEntityTreeItemPtr(const EntityPtr& pEntity, GeneralLibrary::TreeItem* pParent) {
        GeneralLibrary::TreeDataPtr dataPtr {CollectionTreeEntityData::createPtr(pEntity.get())};
        return GeneralLibrary::TreeItem::createPtr(dataPtr, pParent);
    }

} }

