#include "TreeToolkit.hpp"

#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumn.hpp"

namespace Fortah { namespace GitUpdater {

    TreeToolkit::TreeToolkit() = default;

    void TreeToolkit::adjustColumns(QTreeView& pTreeView, const GeneralLibrary::TreeColumns& pColumns) {
        int totalSize {pTreeView.width()};
        int index {0};
        for (const GeneralLibrary::TreeColumnPtr& column : pColumns) {
            pTreeView.setColumnWidth(index, column->getSize().calculate(totalSize));
            pTreeView.setColumnHidden(index, column->isHidden());
            index++;
        }
    }

} }

