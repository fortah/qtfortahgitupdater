#include "CollectionTreeEntityData.hpp"

namespace Fortah { namespace GitUpdater {

    CollectionTreeEntityData::CollectionTreeEntityData(const Entity* pEntity) :
        GeneralLibrary::TreeData(),
        entity {pEntity} {
    }

    CollectionTreeEntityDataPtr CollectionTreeEntityData::createPtr(const Entity* pEntity) {
        return CollectionTreeEntityDataPtr{new CollectionTreeEntityData{pEntity}};
    }

    CollectionTreeEntityData::~CollectionTreeEntityData() = default;

    const Entity* CollectionTreeEntityData::getEntity() const {
        return this->entity;
    }

    GeneralLibrary::TreeDataType CollectionTreeEntityData::getType() const {
        return GeneralLibrary::TreeDataType::Content;
    }

    QVariant CollectionTreeEntityData::getDataItem(int pColumn) const {
        QVariant result;
        switch (pColumn) {
            case 0:
                result = QVariant{this->getEntity()->getName()};
                break;
            case 1:
                result = QVariant{this->getEntity()->getResult().getTypeText()};
                break;
            case 2:
                result = QVariant{this->getEntity()->getResult().getMessage()};
                break;
            default:
                result = QString {};
                break;
        }
        return result;
    }

} }

