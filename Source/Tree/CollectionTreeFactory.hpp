#pragma once

#include "../Data/Collection.hpp"
#include "../Data/Group.hpp"

#include "../../Libraries/FortahGeneralLibrary/Tree/TreeItem.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumns.hpp"

namespace Fortah { namespace GitUpdater {

    class CollectionTreeFactory {

        private:
            CollectionTreeFactory();

        public:
            static GeneralLibrary::TreeColumns createColumns();
            static GeneralLibrary::TreeItemPtr create(const CollectionPtr pCollection, const GeneralLibrary::TreeColumns& pColumns);

        private:
            static GeneralLibrary::TreeItemPtr addCollection(const CollectionPtr pCollection, GeneralLibrary::TreeItem& pParentTreeItem);
            static void addGroups(const Groups& pGroups, GeneralLibrary::TreeItem& pParentTreeItem);
            static void addRepositories(const Repositories& pRepositories, GeneralLibrary::TreeItem& pParentTreeItem);

        private:
            static GeneralLibrary::TreeItemPtr createHeaderTreeItemPtr(const QStringList& pHeaders);
            static GeneralLibrary::TreeItemPtr createEntityTreeItemPtr(const EntityPtr& pEntity, GeneralLibrary::TreeItem* pParent = nullptr);

    };

} }
