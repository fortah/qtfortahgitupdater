#pragma once

#include <QSharedPointer>
#include <QVariant>

#include "../Data/Entity.hpp"

#include "../../Libraries/FortahGeneralLibrary/Tree/TreeData.hpp"
#include "../../Libraries/FortahGeneralLibrary/Tree/TreeDataType.hpp"

namespace Fortah { namespace GitUpdater {

    class CollectionTreeEntityData;
    typedef QSharedPointer<CollectionTreeEntityData> CollectionTreeEntityDataPtr;

    class CollectionTreeEntityData : public GeneralLibrary::TreeData {

        public:
            CollectionTreeEntityData(const Entity* pEntity);
            static CollectionTreeEntityDataPtr createPtr(const Entity* pEntity);
            virtual ~CollectionTreeEntityData();

        private:
            const Entity* entity {nullptr};

        public:
            const Entity* getEntity() const;

        public:
            GeneralLibrary::TreeDataType getType() const;
            QVariant getDataItem(int pColumn) const;

    };

} }
