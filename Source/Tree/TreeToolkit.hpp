#pragma once

#include <QTreeView>

#include "../../Libraries/FortahGeneralLibrary/Tree/TreeColumns.hpp"

namespace Fortah { namespace GitUpdater {

    class TreeToolkit {

        private:
            TreeToolkit();

        public:
            static void adjustColumns(QTreeView& pTreeView, const GeneralLibrary::TreeColumns& pColumns);

    };

} }
