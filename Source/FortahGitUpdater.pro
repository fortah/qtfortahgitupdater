QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    About/AboutModel.cpp \
    About/AboutView.cpp \
    Data/Collection.cpp \
    Data/Entity.cpp \
    Data/EntityResult.cpp \
    Data/EntityResultType.cpp \
    Data/EntityType.cpp \
    Data/Group.cpp \
    Data/Repository.cpp \
    DataProcessing/CollectionFactory.cpp \
    DataProcessing/CollectionReader.cpp \
    DataProcessing/CollectionWriter.cpp \
    General/Application.cpp \
    General/Manifest.cpp \
    Main/MainCollectionView.cpp \
    Main/MainFactory.cpp \
    Main/MainLogic.cpp \
    Main/MainModel.cpp \
    Main/MainNoCollectionView.cpp \
    Main/MainStatusBarHandle.cpp \
    Main/MainView.cpp \
    Tools/InternetToolkit.cpp \
    Tools/LayoutToolkit.cpp \
    Tools/WidgetToolkit.cpp \
    Tree/CollectionTreeEntityData.cpp \
    Tree/CollectionTreeFactory.cpp \
    Tree/TreeToolkit.cpp \
    main.cpp

HEADERS += \
    About/AboutModel.hpp \
    About/AboutView.hpp \
    DataProcessing/CollectionFactory.hpp \
    DataProcessing/CollectionReader.hpp \
    DataProcessing/CollectionWriter.hpp \
    Data/Collection.hpp \
    Data/Entity.hpp \
    Data/EntityResult.hpp \
    Data/EntityResultType.hpp \
    Data/EntityType.hpp \
    Data/Group.hpp \
    Data/Repository.hpp \
    General/Application.hpp \
    General/Manifest.hpp \
    Main/MainCollectionView.hpp \
    Main/MainFactory.hpp \
    Main/MainLogic.hpp \
    Main/MainMode.hpp \
    Main/MainModel.hpp \
    Main/MainNoCollectionView.hpp \
    Main/MainStatusBarHandle.hpp \
    Main/MainView.hpp \
    Tools/InternetToolkit.hpp \
    Tools/LayoutToolkit.hpp \
    Tools/WidgetToolkit.hpp \
    Tree/CollectionTreeEntityData.hpp \
    Tree/CollectionTreeFactory.hpp \
    Tree/TreeToolkit.hpp

FORMS += \
    About/AboutView.ui \
    Main/MainCollectionView.ui \
    Main/MainNoCollectionView.ui \
    Main/MainView.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Libraries/release/ -lFortahGeneralLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Libraries/debug/ -lFortahGeneralLibrary
else:unix: LIBS += -L$$PWD/../Libraries/ -lFortahGeneralLibrary

INCLUDEPATH += $$PWD/../Libraries/FortahGeneralLibrary
DEPENDPATH += $$PWD/../Libraries/FortahGeneralLibrary

RESOURCES += \
    About/General.qrc \
    General.qrc \
    Main/General.qrc

DISTFILES += \
    Node JS Libraries.gco \
    Node JS Libraries.gco \
    NodeJs.json \
    Notes.md
