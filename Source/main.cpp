#include "General/Application.hpp"

int main(int argc, char *argv[]) {
    Fortah::GitUpdater::Application application {argc, argv};
    return application.run();
}
