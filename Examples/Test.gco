{
    "groups": [
        {
            "name": "Applications",
            "path": "Applications",
            "repositories": [
                {
                    "name": "Fortah Archiver",
                    "path": "FortahArchiver",
                    "url": "nodejsfortaharchiver"
                },
                {
                    "name": "Fortah Directory Date Finder",
                    "path": "FortahDirectoryDateFinder",
                    "url": "nodejsfortahdirectorydatefinder"
                },
                {
                    "name": "Fortah Git Updater",
                    "path": "FortahGitUpdater",
                    "url": "nodejsfortahgitupdater"
                },
                {
                    "name": "Fortah Icon Applicator",
                    "path": "FortahIconApplicator",
                    "url": "nodejsfortahiconapplicator"
                },
                {
                    "name": "Fortah Icon Compiler",
                    "path": "FortahIconCompiler",
                    "url": "nodejsfortahiconcompiler"
                },
                {
                    "name": "Fortah Icon Extractor",
                    "path": "FortahIconExtractor",
                    "url": "nodejsfortahiconextractor"
                },
                {
                    "name": "Fortah Icon Maker",
                    "path": "FortahIconMaker",
                    "url": "nodejsfortahicon,aker"
                },
                {
                    "name": "Fortah Image Resizer",
                    "path": "FortahImageResizer",
                    "url": "nodejsfortahimageresizer"
                },
                {
                    "name": "Fortah KDE Plasma Adjuster",
                    "path": "FortahKdePlasmaAdjuster",
                    "url": "nodejsfortahkdeplasmaadjuster"
                }
            ]
        },
        {
            "name": "Libraries",
            "path": "Libraries",
            "repositories": [
                {
                    "name": "Fortah Console Library",
                    "path": "FortahConsoleLibrary",
                    "url": "nodejsfortahconsolelibrary"
                },
                {
                    "name": "Fortah Core Library",
                    "path": "FortahCoreLibrary",
                    "url": "nodejsfortahcorelibrary"
                },
                {
                    "name": "Fortah File System Library",
                    "path": "FortahFileSystemLibrary",
                    "url": "nodejsfortahfilesystemlibrary"
                },
                {
                    "name": "Fortah Git Library",
                    "path": "FortahGitLibrary",
                    "url": "nodejsfortahgitlibrary"
                },
                {
                    "name": "Fortah Image Library",
                    "path": "FortahImageLibrary",
                    "url": "nodejsfortahimagelibrary"
                },
                {
                    "name": "Fortah ImageMagick Library",
                    "path": "FortahImageMagickLibrary",
                    "url": "nodejsfortahimagemagicklibrary"
                },
                {
                    "name": "Fortah Network Library",
                    "path": "FortahNetworkLibrary",
                    "url": "nodejsfortahnetworklibrary"
                },
                {
                    "name": "Fortah Regex Library",
                    "path": "FortahRegexLibrary",
                    "url": "nodejsfortahregexlibrary"
                },
                {
                    "name": "Fortah XML Library",
                    "path": "FortahXmlLibrary",
                    "url": "nodejsfortahxmllibrary"
                },
                {
                    "name": "Fortah Zip Library",
                    "path": "FortahZipLibrary",
                    "url": "nodejsfortahziplibrary"
                }
            ]
        }
    ],
    "branch": "main",
    "name": "Node JS",
    "path": "/home/Development/NodeJS",
    "url": "https://gitlab.com/fortah"
}
